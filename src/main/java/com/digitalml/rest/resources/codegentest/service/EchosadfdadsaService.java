package com.digitalml.rest.resources.codegentest.service;
    	
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import java.net.URL;

import org.apache.commons.collections.CollectionUtils;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.SecurityContext;
import java.security.AccessControlException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.*;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

// Import any model objects used by the interface

import com.digitalml.rest.resources.codegentest.*;

/**
 * Service: Echosadfdadsa
 * #### Echos back every URL, method, parameter and header
Feel free to make a path or an operation and use **Try
Operation** to test it. The echo server will
render back everything.z
sdsf
 *
 * This service has been automatically generated by Ignite
 *
 * @author admin
 * @version 3
 *
 */

public abstract class EchosadfdadsaService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EchosadfdadsaService.class);

	// Required for JSR-303 validation
	static private ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();

	protected static Mapper mapper;

	static {
		URL configFile = EchosadfdadsaService.class.getResource("EchosadfdadsaServiceMappings.xml");
		if (configFile != null) {

			List<String> mappingFiles = new ArrayList<String>();
			mappingFiles.add(configFile.toExternalForm());
			mapper = new DozerBeanMapper(mappingFiles);

		} else {
			mapper = new DozerBeanMapper(); // Use default wildcard mappings only
		}
	}

	protected boolean checkPermissions(SecurityContext securityContext) throws AccessControlException {
		return true;
	}

	/**
	Implements method retrieveTestPath	*/
	public RetrieveTestPathReturnDTO retrieveTestPath(SecurityContext securityContext, RetrieveTestPathInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method retrieveTestPath");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access retrieveTestPath");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access retrieveTestPath");

		RetrieveTestPathReturnDTO returnValue = new RetrieveTestPathReturnDTO();
        RetrieveTestPathCurrentStateDTO currentState = new RetrieveTestPathCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method retrieve	*/
	public RetrieveReturnDTO retrieve(SecurityContext securityContext, RetrieveInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method retrieve");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access retrieve");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access retrieve");

		RetrieveReturnDTO returnValue = new RetrieveReturnDTO();
        RetrieveCurrentStateDTO currentState = new RetrieveCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}

	/**
	Implements method createName	*/
	public CreateNameReturnDTO createName(SecurityContext securityContext, CreateNameInputParametersDTO inputs)  {

		if (LOGGER.isDebugEnabled())
			LOGGER.debug("Entered method createName");

		// Do any security checks
		if (securityContext == null)
			throw new AccessControlException("No SecurityContext available so cannot access createName");

		if (!checkPermissions(securityContext))
			throw new AccessControlException("Insufficient permissions to access createName");

		CreateNameReturnDTO returnValue = new CreateNameReturnDTO();
        CreateNameCurrentStateDTO currentState = new CreateNameCurrentStateDTO();
        
        // Setup the inputs for the first process step
        mapper.map(inputs, currentState.getInputs());
        
		Object returnDTO = null;
		return returnValue;
	}


    // Supporting Use Case and Process methods

	public abstract RetrieveTestPathCurrentStateDTO retrieveTestPathUseCaseStep1(RetrieveTestPathCurrentStateDTO currentState);


	public abstract RetrieveCurrentStateDTO retrieveUseCaseStep1(RetrieveCurrentStateDTO currentState);


	public abstract CreateNameCurrentStateDTO createNameUseCaseStep1(CreateNameCurrentStateDTO currentState);


// Supporting Exception classes

// Supporting DTO classes


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation retrieveTestPath.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class RetrieveTestPathCurrentStateDTO {
		
		private RetrieveTestPathInputParametersDTO inputs;
		private RetrieveTestPathReturnDTO returnObject;
		private RetrieveTestPathReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public RetrieveTestPathCurrentStateDTO() {
			initialiseDTOs();
		}

		public RetrieveTestPathCurrentStateDTO(RetrieveTestPathInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public RetrieveTestPathInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(RetrieveTestPathReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public RetrieveTestPathReturnStatusDTO getErrorState() {
			return errorState;
		}

		public RetrieveTestPathReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new RetrieveTestPathInputParametersDTO();
			returnObject = new RetrieveTestPathReturnDTO();
			errorState = new RetrieveTestPathReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation retrieveTestPath
	 */
	public static class RetrieveTestPathReturnDTO {

	};

	/**
	 * Holds the return value for the operation retrieveTestPath when an exception has been thrown.
	 */
	public static class RetrieveTestPathReturnStatusDTO {

		private String exceptionMessage;

		public RetrieveTestPathReturnStatusDTO() {
		}

		public RetrieveTestPathReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation retrieveTestPath in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class RetrieveTestPathInputParametersDTO {


		private String id;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<RetrieveTestPathInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<RetrieveTestPathInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<RetrieveTestPathInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation retrieve.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class RetrieveCurrentStateDTO {
		
		private RetrieveInputParametersDTO inputs;
		private RetrieveReturnDTO returnObject;
		private RetrieveReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public RetrieveCurrentStateDTO() {
			initialiseDTOs();
		}

		public RetrieveCurrentStateDTO(RetrieveInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public RetrieveInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(RetrieveReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public RetrieveReturnStatusDTO getErrorState() {
			return errorState;
		}

		public RetrieveReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new RetrieveInputParametersDTO();
			returnObject = new RetrieveReturnDTO();
			errorState = new RetrieveReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation retrieve
	 */
	public static class RetrieveReturnDTO {

	};

	/**
	 * Holds the return value for the operation retrieve when an exception has been thrown.
	 */
	public static class RetrieveReturnStatusDTO {

		private String exceptionMessage;

		public RetrieveReturnStatusDTO() {
		}

		public RetrieveReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation retrieve in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class RetrieveInputParametersDTO {



		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<RetrieveInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<RetrieveInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<RetrieveInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};


	/**
	 * Provides a DTO to hold the current state of the orchestration for the operation createName.
	 * This allows the state to be easily passed in method calls.
	 */
	public static class CreateNameCurrentStateDTO {
		
		private CreateNameInputParametersDTO inputs;
		private CreateNameReturnDTO returnObject;
		private CreateNameReturnStatusDTO errorState;
		// DTOs for orchestration
		
		public CreateNameCurrentStateDTO() {
			initialiseDTOs();
		}

		public CreateNameCurrentStateDTO(CreateNameInputParametersDTO inputs) {
			initialiseDTOs();
			this.inputs = inputs;
		}
		
		// Add extra DTOs for steps
		public CreateNameInputParametersDTO getInputs() {
			return inputs;
		}

		
		public void setErrorState(CreateNameReturnStatusDTO errorState) {
			this.errorState = errorState;
		}

		public CreateNameReturnStatusDTO getErrorState() {
			return errorState;
		}

		public CreateNameReturnDTO getReturnObject() {
			return returnObject;
		}
		
		private void initialiseDTOs() {
			inputs = new CreateNameInputParametersDTO();
			returnObject = new CreateNameReturnDTO();
			errorState = new CreateNameReturnStatusDTO();
		
		}			
	};

	/**
	 * Holds the return value for the operation createName
	 */
	public static class CreateNameReturnDTO {

	};

	/**
	 * Holds the return value for the operation createName when an exception has been thrown.
	 */
	public static class CreateNameReturnStatusDTO {

		private String exceptionMessage;

		public CreateNameReturnStatusDTO() {
		}

		public CreateNameReturnStatusDTO(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}



		public String getExceptionMessage() {
			return exceptionMessage;
		}

		public void setExceptionMessage(String exceptionMessage) {
			this.exceptionMessage = exceptionMessage;
		}
	};

	/**
	 * Holds the input parameters for the operation createName in a single DTO which aids
	 * validation and allows the inputs to be easily passed in method calls.
	 */
	public static class CreateNameInputParametersDTO {


		private String name;

		private String year;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getYear() {
			return year;
		}

		public void setYear(String year) {
			this.year = year;
		}


		public boolean validate() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<CreateNameInputParametersDTO>> errors = validator.validate(this);
			return CollectionUtils.isEmpty(errors);
		}

		public List<String> validateReport() {
			Validator validator = validatorFactory.getValidator();
			Set<ConstraintViolation<CreateNameInputParametersDTO>> errors = validator.validate(this);

			List<String> results = new ArrayList<String>();
			if (CollectionUtils.isNotEmpty(errors))
				for (ConstraintViolation<CreateNameInputParametersDTO> error : errors) {
					StringBuffer sb = new StringBuffer();
					sb.append(error.getMessage());
					results.add(sb.toString());
				}

			return results;
		}
	};

}