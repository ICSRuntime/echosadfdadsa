package com.digitalml.rest.resources.codegentest.service.Echosadfdadsa;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.security.Principal;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.ws.rs.core.SecurityContext;
import com.google.common.base.Strings;

import com.digitalml.rest.resources.codegentest.service.EchosadfdadsaService;
	
/**
 * Default implementation for: Echosadfdadsa
 * #### Echos back every URL, method, parameter and header
Feel free to make a path or an operation and use **Try
Operation** to test it. The echo server will
render back everything.z
sdsf
 *
 * @author admin
 * @version 3
 */

public class EchosadfdadsaServiceDefaultImpl extends EchosadfdadsaService {


    public RetrieveTestPathCurrentStateDTO retrieveTestPathUseCaseStep1(RetrieveTestPathCurrentStateDTO currentState) {
    

        RetrieveTestPathReturnStatusDTO returnStatus = new RetrieveTestPathReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: #### Echos back every URL, method, parameter and header Feel free to make a path or an operation and use **Try Operation** to test it. The echo server will render back everything.z sdsf");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public RetrieveCurrentStateDTO retrieveUseCaseStep1(RetrieveCurrentStateDTO currentState) {
    

        RetrieveReturnStatusDTO returnStatus = new RetrieveReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: #### Echos back every URL, method, parameter and header Feel free to make a path or an operation and use **Try Operation** to test it. The echo server will render back everything.z sdsf");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };


    public CreateNameCurrentStateDTO createNameUseCaseStep1(CreateNameCurrentStateDTO currentState) {
    

        CreateNameReturnStatusDTO returnStatus = new CreateNameReturnStatusDTO();
    	returnStatus.setExceptionMessage("Exception thrown in Step. The Error code is: . Description: #### Echos back every URL, method, parameter and header Feel free to make a path or an operation and use **Try Operation** to test it. The echo server will render back everything.z sdsf");
	   	currentState.setErrorState(returnStatus);
	   	return currentState;
		        };

	/**
	 * Creates and returns a {@link Method} object using reflection and handles the possible exceptions.
	 * <br/>
	 * Aids with calling the process step method based on the outcome of the control logic
	 * 
	 * @param methodName
	 * @param clazz
	 * @return
	 */
	private Method getMethod(String methodName, Class clazz) {
		Method method = null;
		try {
			method = EchosadfdadsaService.class.getMethod(methodName, clazz);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		return method;
	}
	
	/**
	 * For use when calling provider systems.
	 * <p>
	 * TODO: Implement security logic here
	 */
	protected SecurityContext securityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}