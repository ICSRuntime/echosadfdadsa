package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Echosadfdadsa.EchosadfdadsaServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.EchosadfdadsaService.RetrieveTestPathInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfdadsaService.RetrieveTestPathReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveTestPathTests {

	@Test
	public void testOperationRetrieveTestPathBasicMapping()  {
		EchosadfdadsaServiceDefaultImpl serviceDefaultImpl = new EchosadfdadsaServiceDefaultImpl();
		RetrieveTestPathInputParametersDTO inputs = new RetrieveTestPathInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		RetrieveTestPathReturnDTO returnValue = serviceDefaultImpl.retrieveTestPath(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}