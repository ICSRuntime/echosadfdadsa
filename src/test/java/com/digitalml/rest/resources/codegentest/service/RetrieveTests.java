package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Echosadfdadsa.EchosadfdadsaServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.EchosadfdadsaService.RetrieveInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.EchosadfdadsaService.RetrieveReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class RetrieveTests {

	@Test
	public void testOperationRetrieveBasicMapping()  {
		EchosadfdadsaServiceDefaultImpl serviceDefaultImpl = new EchosadfdadsaServiceDefaultImpl();
		RetrieveInputParametersDTO inputs = new RetrieveInputParametersDTO();
		RetrieveReturnDTO returnValue = serviceDefaultImpl.retrieve(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}